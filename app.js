/* eslint-disable no-console */

module.exports = () => {
  const csv = require('csv');
  const csvObj = csv();

  const getIndex = (obj, prop, value) => obj.map(e => e[prop]).indexOf(value);

  class Order {
    constructor(id, date, amount) {
      this.id = id;
      this.date = date;
      this.amount = amount;
    }
  }

  class Orders {
    constructor() {
      this.orders = [];
    }

    loadFromCSV(path) {
      return new Promise((resolve, reject) => {
        csvObj.from.path(path).to.array((data) => {
          if (data) {
            data.forEach((entry, i) => {
              this.orders.push(new Order(
                Number(data[i][0]), new Date(data[i][1]), Number(data[i][2]),
              ));
            });
            resolve(this.orders);
          } else reject(new Error('something is wrong', data));
        });
      });
    }


    getAll() {
      return this.orders;
    }
  }

  class Customer {
    constructor(id, date, amount) {
      this.id = id;

      this.orders = [];
      this.addOrder(date, amount);
    }


    addOrder(date, amount) {
      this.orders.push(
        {
          month: new Date(date).getMonth(),
          amounts: [{ amount, date }],
        },
      );
    }

    setOrderAmount(id, amount, date) {
      this.orders[id].amounts.push({ amount, date });
    }
  }

  class Customers {
    constructor() {
      this.customers = [];
    }

    addCustomer(id, date, amount) {
      this.customers.push(new Customer(id, date, amount));
    }

    getAll() {
      return this.customers;
    }

    getCustomer(id) {
      return this.customers[id];
    }
  }

  const csvPath = process.argv.slice(2)[0];

  const ordersFromCsv = new Orders();

  ordersFromCsv.loadFromCSV(csvPath).then(() => {
    const customers = new Customers();

    ordersFromCsv.getAll().forEach((order) => {
      const { id, date, amount } = order;
      const customerId = getIndex(customers.getAll(), 'id', id);

      // no customers with this id
      if (customerId === -1) {
        customers.addCustomer(id, date, amount);
      } else {
        const customer = customers.getCustomer(customerId);
        const orderId = getIndex(customer.orders, 'month', date.getMonth());
        // no orders in this month
        if (orderId === -1) {
          customer.addOrder(date, amount);
        } else {
          customer.setOrderAmount(orderId, amount, date);
        }
      }
    });

    customers.getAll().forEach((cst) => {
      const total = [];

      // sort orders by month for rule A
      cst.orders.sort((a, b) => a.month - b.month);

      cst.orders.forEach((order, i) => {
        // total points per month
        const points = [];

        // sort month amounts by date for rule B
        order.amounts.sort((a, b) => a.date - b.date);
        total.push(0);

        order.amounts.forEach((o, j) => {
          points.push(0);
          // total amount per month
          total[i] += o.amount;

          // rule A
          if (total[i - 1] && total[i - 1] > 2000 && o.amount > 100) {
            points[j] = 5;
          }

          // rule B
          if ((j + 1) % 3 === 0) {
            points[j] = 10;
          }

          // rule C
          if (o.amount > 1000) {
            points[j] = 20;
          }
        });

        // final result
        console.log(`${cst.id},2018-0${order.month + 2}-01,${points.reduce((a, b) => a + b, 0)}`);
      });
    });
  });
};
